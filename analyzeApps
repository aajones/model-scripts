#!/usr/local/bin/node

/**
Inspect for types in apps
**/

var commander = require('commander');
var fs = require('fs');
var request = require('request');
var url = require('url');

commander
    .version('0.0.1')
    .option('-u, --user <username>', 'username')
    .option('-p, --pass <password>', 'password')
    .option('-h, --host <hostname>', 'hostname')
    .option('-n, --ns <namespace>', 'namespace')
    .option('-f, --fmt [format]', 'format (csv (default), json)')
    .option('-o, --out [outfile]', 'output file')
    .option('-c, --chief', 'Invoke the Chief')
    .parse(process.argv);

//TODO: abstract this out
var env = {
	"hostname": commander.host,
	"username": commander.user,
	"password": commander.pass,
	"namespace": commander.ns
};

var state = {
    applicationsQueryCount: 0,
    applications: [],
    report: []
};

var main = function() {

    if (commander.chief) {
        console.log('       ');
        console.log(' ///// ');
        console.log('| O O |');
        console.log('|  ^  |  Working on it...');
        console.log('| --- |');
        console.log('   #   ');
        console.log('       ');
    }

    var start = function() {
        getData(env, "/resources/BodhiApplication/count", {}, function(localState,body) {

            var count = parseInt(body.count/100);
            if (body.count % 100 > 0) {
                count++;
            }
            state.applicationsQueryCount = count;

            getApplications();
        });
    };

    //types
    var getApplications = function() {
        var counter = 0;
        for (var i=0; i<state.applicationsQueryCount;i++) {
            var page = i + 1;
            var path = '/resources/BodhiApplication?paging=limit:100,page:'+page;
            getData(env, path, {}, function(localState,body) {
                state.applications = state.applications.concat(body);
                counter++;
                if (counter == state.applicationsQueryCount) {
                    processApps();
                }
            });
        }
    };

    var processApps = function() {
        for (var i=0; i<state.applications.length; i++) {
            var app = state.applications[i];
            processApp(app);
        }
        finish();
    };

    var processApp = function(app) {

        if (!app.hasOwnProperty("settings")) {
            return;
        }

        if (!app.settings.hasOwnProperty("install")) {
            return;
        }

        if (!app.settings.install.hasOwnProperty("new")) {
            return;
        }

        if (!app.settings.install['new'].hasOwnProperty("model")) {
            return;
        }

        if (app.settings.install['new'].model.length == 0) {
            return;
        }

        var model = app.settings.install['new'].model;

        for (var i=0; i<model.length; i++) {

            var m = model[i];

            var usage = {
                'type': m.type,
                'name': m.name,
                'app': app.name
            };

            state.report.push(usage);

        }

    };

    var csvMe = function(input) {
        var out = '';
        out += ["Type","Name","App"].join(',') + '\n';
        for (var i=0; i<input.length; i++) {
            out += [input[i].type , input[i].name , input[i].app].join(',') + '\n';
        }
        return out;
    };

    var finish = function() {

        var out = '';

        if (commander.fmt == 'csv') {
            out = csvMe(state.report);
        } else {
            out = state.report;
        }

        if (commander.out != null) {
            fs.writeFileSync(commander.out, out);
        } else {
            console.log(out);
        }

        if (commander.chief) {
            console.log('       ');
            console.log(' ///// ');
            console.log('| @ @ |');
            console.log('|  ^  | All Good!');
            console.log('| --- |');
            console.log('   #   ');
            console.log('       ');
        }

    };

    start();

};

//TODO: loop through EmbeddedTypes, find usage

/**
 * Helper to GET data from API
 * @param env
 * @param path
 * @param state
 * @param callback
 */
var getData = function(env, path, localState, callback) {

    var uri = "/"+ env.namespace + path;

    var absoluteUrl = url.resolve(env.hostname, uri);
    request({
        method: 'GET',
        uri: absoluteUrl,
        auth: {
            user    : env.username,
            password: env.password
        },
        json: true
    }, function(error, response, body) {
        if (error) return;

        //console.log('GET successful!', response.statusCode, uri);

        callback(localState, body);

    });

};

main();

